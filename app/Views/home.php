<?= $this->extend('layout') ?>

<!-- ========= TITLE PAGE  ========= -->
<?= $this->section('title-page') ?>Home<?= $this->endSection() ?>


<?= $this->section('content') ?>

    <!-- ========= Popups Dinámicos ========= -->
    <?= $this->include('partials/popup') ?>

    <div class="home-swiper">
        <!-- Additional required wrapper -->
        <div class="swiper-wrapper" id="home-wrapper">
            <!-- Slides -->
            <?php foreach ($slides as $slide): ?>
                <!-- Verificar si el slide actual tiene fecha de cierre programada  -->
                <?php if ($slide["end_date"]): ?>
                    <?php if (strtotime($slide["end_date"]) >= time()): ?>
                        <div class="swiper-slide" style="background-image: url('<?= $mobile ? base_url($slide['img_mobile']) : base_url($slide['img_desktop']); ?>')"></div>
                    <?php endif; ?>
                <?php else: ?>
                    <div class="swiper-slide" style="background-image: url('<?= $mobile ? base_url($slide['img_mobile']) : base_url($slide['img_desktop']); ?>')"></div>
                <?php endif; ?>
            <?php endforeach; ?>

           <?php /*
            <?php foreach ($slides as $slide): ?>
                <?php if ($mobile): ?>
                    <div class="swiper-slide" style=" background-image: url('<?= base_url($slide['img_mobile']) ?>') "></div>
                <?php else: ?>
                    <div class="swiper-slide" style=" background-image: url('<?= base_url($slide['img_desktop']) ?>') "></div>
                <?php endif; ?>
            <?php endforeach; ?>
            */ ?>
        
        </div>
        <div class="d-flex justify-content-between main-scroll-down">
            <div></div>
            <div class='text-center'><i class="fa-solid fa-angle-down bounce" alt="Scroll Down" id="scroll-down"></i></div>
            <div><div class="swiper-pagination"></div></div>
        </div>
    </div>


    <section class="about-loyalty container" id="about-loyalty">
        <div class="row">
            <div class="col-sm-12 col-md-8">
                <div class="about-description d-flex align-items-center h-100">
                    <div>
                        <h1 class="section-title deco-line">PROGRAMA DE FIDELIDAD</h1>
                        <p>Te presentamos Somos Pla·Sa el programa para los que somos especiales y únicos.</p>
                        <p>En Plaza Satélite somos amigos, somos nuestros recuerdos, somos nuestros valores, somos los lugares que ya son nuestros. Somos los que paseamos en el parque, los que celebramos nuestras alegrías y los que nos reencontramos con amigos. Por todo esto y más, tú y yo somos parte de esta comunidad.</p>
                        <p>Únete a nuestro programa de fidelidad Somos Pla·Sa y déjanos consentirte con beneficios y premios exclusivos para ti.</p>
                        <a href="<?= base_url('/conocenos');?>" class="btn btn-custom">Conoce más</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-4 d-flex align-items-center">
                <img src="<?= base_url('assets/images/home-cards.png') ?>" alt="Compra exitosa" class='img-fluid d-none d-md-block'>
            </div>
        </div>
    </section>


    <div class="bg-gray">
        <section class="container">
            <h2 class="section-title text-center deco-line-center space-2">LO NUEVO</h2>
                <?php if ($premios): ?>
                    <div class="news-swiper">
                        <div class="swiper-wrapper" id="news-wrapper">
                            <?php foreach ($premios as $premio): ?>
                                <div class="swiper-slide">
                                    <div class="card-wrap">
                                        <div class="certificate">
                                            <img src="<?= 'https://concierge.somosplasa.com/' . $premio['image'] ?>" class="img-fluid certificate__image" alt="<?= strtoupper(esc($premio['title'])) ?>" title="<?= strtoupper(esc($premio['title'])) ?>">
                                            <div class="certificate__info">
                                                <h5 class="certificate__title"><?= esc($premio['title']) ?></h5>
                                                <h6 class="certificate__subtitle"><?= esc($premio['subtitle']) ?></h6>
                                                <ul class="p-0 list-unstyled certificate__list">
                                                    <li class="certificate__item"><i class="fas fa-coins"></i> Puntos: <?= number_format(esc($premio['points']),0,'.', ',') ?></li>
                                                    <li class="certificate__item"><i class="fas fa-cubes"></i> Stock: <?= esc($premio['current_stock']) ?></li>
                                                </ul>
                                                <p class="certificate__description"><?= esc($premio['description']) ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                    
                    <!-- <div class="d-flex justify-content-center mt-4">
                        <div class="more-button">
                            <a href="<?= base_url('premios');?>" class="btn btn-custom">Ver todos</a>
                        </div>
                    </div> -->
                <?php else: ?>
                    <div class="col-12 text-center">
                        <img src="<?= base_url('assets/images/svg/404-gifts.svg')?>" alt="No hay premios registrados" class="img-fluid w-50 mb-4">
                        <h2 class="listado-premios__title-error">Upss!</h2>
                        <p class="listado-premios__message">Coméntale al cliente que todos los certificados han volado, son muchas las recompensas que se obtienen al estar registrado en el Plan de Lealtad.
                            <br>Pero este suceso no debe preocuparle. <strong>Mañana habrá mas certificados para canjerar.</strong></p>
                    </div>
                <?php endif; ?>
        </section>
    </div>


    <section class="my-5 benefits-home-section container" style="background-image: url('<?= base_url('assets/images/somos-banner.jpg') ?>') ">
        
    </section>
    <div class="space"></div>

<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<link rel="stylesheet" href="<?= base_url('assets/css/popup.css'); ?>">
<link rel="stylesheet" href="<?= base_url('assets/vendor/fancybox/jquery.fancybox.min.css') ?>">
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script src="<?= base_url('assets/vendor/jquery/jquery-3.6.0.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/fancybox/jquery.fancybox.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/moment/moment-with-locales.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/moment/moment-timezone.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/moment/moment-timezone-with-data.min.js'); ?>"></script>
<!-- Lógica Popups Dinámicos -->
<?php if ($popup != null && is_object($popup)): ?>
    <script>
        const current_time = moment().tz("America/Mexico_City").format("YYYY-MM-DD HH:mm:ss");

        const date_open = '<?php echo $popup->start_date ?>';
        const date_close = '<?php echo $popup->end_date ?>';

        let popupvisible = false;
        let isMobile = false;

        // Verificar si el popup aun debe estar visible en el sitio
        if (current_time >= date_open && current_time <= date_close){
            popupvisible = true;
        }

        // Verificar si el sitio esta siendo consumido por algún dispositivo movil
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ){
            isMobile = true;
        }

        // Mostrar el popup correspondiente [Desktop || Mobile]
        if(popupvisible){
            if(isMobile === false){
                setTimeout(function(){
                    $.fancybox.open({src: '#popup_desktop'});
                }, 1000);
            } else {
                setTimeout(function(){
                    $.fancybox.open({src: '#popup_mobile'});
                }, 1000);
            }
        }
    </script>
<?php endif; ?>
<?= $this->endSection() ?>