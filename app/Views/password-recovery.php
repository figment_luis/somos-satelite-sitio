<?= $this->extend('layout') ?>

<!-- ========= TITLE PAGE  ========= -->
<?= $this->section('title-page') ?>Actualizar Contraseña<?= $this->endSection() ?>


<?= $this->section('content') ?>

    <div class="bg-login d-flex align-items-center pt-5">
        <div class="container">
            <div class="recovery-form">
                <?php if($client_id): ?>
                    <form id="set-new-password" name="set-new-password" method="post" backlink="<?= base_url()?>" class="login-form text-center py-5 px-3">
                        <div class="my-3 text-center">
                            <img src="<?= base_url('assets/images/logotipo.png')?>" alt=""class="img-fluid text-center">
                        </div>
                    
                        <h2 class="text-center mb-5">Ingresa tu nueva contraseña</h2>
                        <div class="form-group mb-1">
                            <input 
                                type="password" 
                                class="form-control" 
                                id="client_password" 
                                name="client_password" 
                                placeholder="nueva contraseña"
                                minlength="8" 
                                required>
                        </div>
                        <div class="form-group">
                            <input 
                                type="password" 
                                class="form-control" 
                                id="client_password_confirm" 
                                name="client_password_confirm" 
                                placeholder="confirma tu nueva contraseña"
                                minlength="8" 
                                required>
                        </div>
                        <input type="hidden" name="client_id" value="<?= $client_id ?>">
                        <button type="submit" class="mt-3 btn btn-custom">Actualizar Contraseña</button>
                    </form>
                <?php else: ?>
                        <div class="text-center">
                            <div class="my-3 text-center login-image">
                                <img src="<?= base_url('public/images/main-logo.png')?>" alt="">
                            </div>
                            <p>Lo sentimos este enlace ha caducado</p>
                            <a href="<?= base_url() ?>" class="btn btn-custom">Ir al Inicio</a>
                        </div>
                        
                <?php endif; ?>
            </div>
        </div>
    </div>


<?= $this->endSection() ?>