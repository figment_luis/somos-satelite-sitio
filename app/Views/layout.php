<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?= base_url('assets/images/favicon.png'); ?>" type="image/x-icon">
    <title><?= $this->renderSection('title-page') ?></title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/normalize.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/swiper-bundle.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/sweetalert2.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/fonts.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/fullscreenmenu.css'); ?>">
    <?= $this->renderSection('styles') ?>
    <link rel="stylesheet" href="<?= base_url('assets/css/styles.css'); ?>">
    
</head>
<body class="d-flex flex-column min-vh-100">

    <!-- ========= NAVBAR  ========= -->
	<?= $this->include('navbar') ?>
    
    <!-- ========= MAIN CONTENT  ========= -->
    <?= $this->renderSection('content') ?>

    
    <div class="text-center" id="scroll-up">
        <i class="fa-solid fa-angle-up"></i>
    </div>


    <!-- ========= FOOTER  ========= -->
    <?= $this->include('footer') ?>

    <script src="<?= base_url('assets/js/bootstrap.bundle.min.js')?>"></script>
    <script src="<?= base_url('assets/js/swiper-bundle.js')?>"></script>
    <script src="<?= base_url('assets/js/sweetalert2.all.min.js')?>"></script>
    <?= $this->renderSection('scripts') ?>
    <script id="main-js" src="<?= base_url('assets/js/main.js')?>" url="<?= base_url(); ?>"></script>
    
</body>
</html>