<?= $this->extend('layout') ?>

<!-- ========= TITLE PAGE  ========= -->
<?= $this->section('title-page') ?>Conócenos<?= $this->endSection() ?>


<?= $this->section('content') ?>
        <div class="seccion-conocenos bg-gray">
            <section class="container">
                <h1 class="section-title text-center deco-line-center">Conócenos</h1>
                <div class="row mt-5">
                    <div class="col-sm-12 col-md-6">
                        <div class="d-flex flex-column justify-content-center h-100">
                            <h3 class="subtitle">¿QUÉ ES SOMOS PLA·SA?</h3>
                            <p>Es un programa de fidelidad para los clientes de Plaza Satélite que te da ventajas y privilegios únicos para ti.</p>
                            <br>
                        </div>
                        
                    </div>
                    <div class="col-sm-12 col-md-6 text-center">
                        <img src="<?= base_url('assets/images/conocenos-1.jpg');?>" alt="" class="img-fluid">
                    </div>
                </div>
            </section>
        </div>
        

        <section class="container steps-section">
            <h1 class="section-title text-center mb-5 deco-line-center">¿Cómo funciona?</h1>
            <div class="row mb-5">
                <div class="col-sm-12 col-md-4 d-none d-md-block">
                    <img src="<?= base_url('assets/images/conocenos-2.jpg');?>" alt="" class="img-fluid">
                </div>
                <div class="col-sm-12 col-md-6 offset-md-2">
                    <div class="d-flex flex-column justify-content-end h-100">
                        <div class="d-flex align-items-center justify-content-center justify-content-md-start">
                            <span class="step-title"><i class="fa-solid fa-pen-to-square"></i></span>
                            <p class="step-description">Inscríbete por medio de nuestro sitio web o directamente en el módulo de información. </p>
                        </div>
                        <div class="d-flex align-items-center justify-content-center justify-content-md-start">
                            <span class="step-title"><i class="fa-solid fa-bag-shopping"></i></span>
                            <p class="step-description">Recibe tu tarjeta y regalo de bienvenida al momento de tu inscripción. </p>
                        </div>

                        <div class="d-flex align-items-center justify-content-center justify-content-md-start">
                            <span class="step-title"><i class="fa-solid fa-receipt"></i></span>
                            <p class="step-description">Registra tus compras y acumula puntos. </p>
                        </div>
                        
                        <div class="d-flex align-items-center justify-content-center justify-content-md-start">
                            <span class="step-title"><i class="fa-solid fa-gifts"></i></span>
                            <p class="step-description">Canjea tus puntos por premios exclusivos para ti. </p>
                        </div>
                        
                        <div class="d-flex align-items-center justify-content-center justify-content-md-start">
                        <span class="step-title"><i class="fa-solid fa-gift"></i></span>
                            <p class="step-description">Evoluciona al siguiente nivel para tener mayores beneficios. </p>
                        </div>
                        
                        <br>
                        <p>La suscripción a este programa es rápida y gratuita.</p>
                    </div>
                </div>
            </div>
        </section>

        <div class="container"><hr></div>
        

        <section class="container card-info-section">
            <h1 class="section-title text-center deco-line-center mb-5">Niveles y beneficios</h1>
            <div class="row">
                <?php foreach($cards as $card):?>
                    <div class="col-sm-12 col-md-4 mb-3">
                        <img src="<?= 'https://concierge.somosplasa.com/' . $card['image'] ?>" alt="" class="img-fluid mb-5">
                        <h2 class="mt-2 card-info-title"><?= $card['name']?></h2>
                        <p><?= $card['description']?></p>
                        <br><br>
                    </div>
                <?php endforeach;?>
            </div>
        </section>
        
<?= $this->endSection() ?>
