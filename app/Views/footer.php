<footer class='main-footer mt-auto'>
    <div class="container py-4">

        <div class="footer-logo text-center">
            <img src="<?= base_url('assets/images/main-logo-white.svg')?>" alt="Logo Footer" class="img-fluid">
        </div>
        <nav class="d-block d-md-flex justify-content-between my-5">
            <a class="link-line" href="<?= base_url()?>">Plaza Satélite</a>
            <a class="link-line" href="<?= base_url('premios')?>">Premios</a>
            <a class="link-line" href="<?= base_url('beneficios')?>">Beneficios</a>
            <a class="link-line" href="<?= base_url('conocenos')?>">Conócenos</a>
            <a class="link-line" href="<?= base_url('contacto')?>">Contacto</a>
            <a class="link-line" href="<?= base_url('faqs')?>">FAQS</a>
            <?php if($session == 1): ?>
                <a href="<?= base_url('estado-de-cuenta')?>">Iniciar sesión</a>
            <?php else:?>
                <a class="link-line" data-toggle="modal" data-bs-toggle="modal" data-bs-target="#loginModal">Iniciar sesión</a>
            <?php endif;?>
        </nav>

        <p class="text-center pt-3">Síguenos en nuestras redes sociales</p>

        <div class="d-flex justify-content-between footer-social-icons pt-3 pb-5 mb-4">
            <a href="#"><i class="fa-brands fa-facebook-f"></i></i></a>
            <a href="#"><i class="fa-brands fa-instagram"></i></a>
            <a href="#"><i class="fa-brands fa-twitter"></i></a>
        </div>

        <p class="text-center"><a class="text-underline" href="#">Aviso de privacidad</a></p>
        <p class="text-center"><a class="text-underline" href="<?= base_url('faqs')?>">FAQs</a></p>
        <p class="text-center">© 2022 PLAZA SATÉLITE.</p>
    
    </div>
</footer>