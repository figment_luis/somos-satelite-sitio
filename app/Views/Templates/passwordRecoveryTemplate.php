<!DOCTYPE html>
<html lang="es-MX">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Loyalty - Recuperar contraseña</title>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Lato&family=Open+Sans:wght@700&family=Roboto+Serif&display=swap');
        * {
            box-sizing: border-box;
            margin: 0;
            padding: 0;
            list-style: none;
            
            border: 0;
            outline: none;
        }
        body {
            font-size: 16px;
            font-family: 'Lato', sans-serif;
            min-height: 100vh;
            background-color: #efefef;
            color: #8b8e92;
            display: flex;
            justify-content: center;
            align-items: center;
            
           
        }
        .page__container {
            max-width: 768px;
            padding: 1rem;
        }
        .page__title {
            color: #06a3c2;
            font-size: 1.5rem;
            text-transform: uppercase;
            margin-bottom: 2rem;
            text-align: center;
        }
        .page__link {
            color: #06a3c2;
        }
        .page__description {
            margin-bottom: 1rem;
            line-height: 1.5;
        }
        .page__logotipo {
            display: block;
            /*margin: 0 auto;*/
            
            width: 150px;
            height: auto;
            object-fit: cover;
        }
        .page__image {
            display: block;
            margin: 0 auto 2rem;
            height: 400px;
            max-width: 100%;
        }
        .page__footer {
           
            margin-top: 3rem;
        }
        .page__footer p {
            text-transform: uppercase;
            margin-bottom: 1.5rem;
        }
    </style>
</head>
<body>
    <div class="page">
        <div class="page__container">
            <img src="<?= base_url('assets/images/recovery_password.svg') ?>" alt="Loyalty" class="page__image">
            <h1 class="page__title">Recuperación de contraseña</h1>
            <p class="page__description">Tiene 3 días para poder cambiar su contraseña, después de ese tiempo el sistema bloqueará el enlace y tendrá que volver a solicitar la recuperación de contraseña. Recuerde que la contraseña distingue entre mayúsculas y minúsculas.</p>
            <p class="page__description">Click en el siguiene enlace para recuperar su contraseña:</p>
            <a href="<?= base_url('update-password/'.$hash) ?>" class="page__link"><?= base_url('update-password/'.$hash) ?></a>
            <footer class="page__footer">
                <p>Atentamente, el equipo</p>
                <img src="<?= base_url('assets/images/logotipo.png') ?>" alt="Loyalty" class="page__logotipo">
            </footer>
        </div>
    </div>
</body>
</html>