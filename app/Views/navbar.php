<div class="main-navbar sticky-top shadow" id="main-navbar">
    <div class="container d-flex justify-content-between align-items-center h-100">
      <div class="navbar-session-btn d-none d-md-block" id="navbar-session-btn">
          <?php if($session == 1): ?>
                <a class="<?=($uri->getSegment(1)==='estado-de-cuenta')?'active':''?>" href="<?= base_url('estado-de-cuenta')?>">Estado de cuenta</a>
                <!-- <a href="<?= base_url('logout')?>"><i class="fa-solid fa-power-off"></i> Cerrar sesión</a> -->
          <?php else:?>
                <a href="" data-toggle="modal" data-bs-dismiss="modal" data-bs-toggle="modal" data-bs-target="#loginModal">Iniciar sesión</a>
          <?php endif;?>
        </div>
        
        
        <div class="logo" id="navbar-logo">
            <a href="<?= base_url(); ?>"><img src="<?= base_url('assets/images/main-logo.svg')?>" alt="Logotipo Plan de Lealtead" class='img-fluid'></a>
        </div>

        <div class="navbar-menu" id="navbar-menu">
            <span></span>
            <span></span>
            <span></span>
        </div>

        
    </div>
</div>

<!-- Login Modal -->

<div class="modal fade" id="loginModal" tabindex="-1" aria-labelledby="loginModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="loginModalLabel">Acceder a tu cuenta</h5>
        <button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form id="loginForm">
            <div class="mb-3">
              <label for="email" class="form-label">Correo electrónico</label>
              <input type="email" class="form-control" id="email" name="email" required>
            </div>
            <div class="mb-3">
              <label for="password" class="form-label">Contraseña</label>
              <input type="password" class="form-control" id="password" name="password" required autocomplete="on">
            </div>
            <div class='d-flex justify-content-center'>
              <button type="submit" class="btn btn-custom">Entrar</button>
            </div>
            <br>
            <div class="d-flex justify-content-center">
              <p id="link-modal-password" data-bs-dismiss="modal" data-toggle="modal" data-bs-toggle="modal" data-bs-target="#passwordRecoveryModal">¿Olvidaste tu contraseña?</p>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="passwordRecoveryModal" tabindex="-1" aria-labelledby="passwordRecoveryLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="passwordRecoveryLabel">Recuperar Contraseña</h5>
        <button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form id="passwordRecoveryForm">
            <div class="mb-3">
              <label for="client_email" class="form-label">Ingresa tu correo electrónico</label>
              <input type="email" class="form-control" id="client_email" name="client_email" required>
            </div>
            <div class='d-flex justify-content-center'>
              <button type="submit" class="btn btn-custom">Recuperar contraseña</button>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>


<div class="menu-content" id="menu-content">
      <div class="container menu-list d-flex align-items-center justify-content-center pb-5">
        <div class="d-flex flex-column align-items-center">
            <a class="<?=($uri->getSegment(1)==='')?'active':''?>" href="<?= base_url()?>">INICIO</a>
            <a class="<?=($uri->getSegment(1)==='conocenos')?'active':''?>" href="<?= base_url('conocenos')?>">Conócenos</a>
            <a class="<?=($uri->getSegment(1)==='premios')?'active':''?>" href="<?= base_url('premios')?>">Premios</a>
            <a class="<?=($uri->getSegment(1)==='beneficios')?'active':''?>" href="<?= base_url('beneficios')?>">Beneficios</a>
            <a class="<?=($uri->getSegment(1)==='contacto')?'active':''?>" href="<?= base_url('contacto')?>">Contacto</a>
            <div class="d-block d-md-none text-center">
                <?php if($session == 1): ?>
                    <a class="<?=($uri->getSegment(1)==='estado-de-cuenta')?'active':''?>" href="<?= base_url('estado-de-cuenta')?>">Estado de cuenta</a>
                    <!-- <a href="<?= base_url('logout')?>"><i class="fa-solid fa-power-off"></i> Cerrar sesión</a> -->
                <?php else:?>
                    <a href="" data-toggle="modal" data-bs-dismiss="modal" data-bs-toggle="modal" data-bs-target="#loginModal">Iniciar sesión</a>
                <?php endif;?>
            </div>
            <div class="nav-deco-line"></div>
            <div class="d-flex justify-content-between navbar-social">
              <a href="#"><i class="fa-brands fa-facebook-f"></i></i></a>
              <a href="#"><i class="fa-brands fa-instagram"></i></a>
              <a href="#"><i class="fa-brands fa-twitter"></i></a>
            </div>
        </div>
      </div>
</div>