<?= $this->extend('layout') ?>

<!-- ========= TITLE PAGE  ========= -->
<?= $this->section('title-page') ?>Estado de cuenta<?= $this->endSection() ?>


<?= $this->section('content') ?>

   <div class="account-bg">
      <div class="account-content-bg"></div>
      <div class="container">
         <div class="row">
            <div class="col-sm-12 col-md-12 col-xl-6 account-data">
                  <h2 class="deco-line line-color-main">ESTADO DE CUENTA</h2>
                  <br><br>

                  <h5><?= $cliente['nombre']; ?></h5>
                  <p>Email: <span><?= $cliente['email']; ?></span> </p>
                  <p>Tarjeta: <span><?= $cliente['tarjeta']; ?></span></p>
                  <p>Nivel: <span><?= $cliente['nivel']; ?></span> </p>
                  <p>Puntos disponibles: <span> <?= ($puntos['points_available']) ? $puntos['points_available'] : 0 ?> </span></p>
                  <p>Puntos redimidos: <span> <?= ($puntos['points_redeemed']) ? $puntos['points_redeemed'] : 0 ?> </span></p>
                  <p>Puntos en deuda: <span> <?= ($puntos['points_debt']) ? $puntos['points_debt'] : 0 ?> </span></p>

                  <form action="<?= base_url('logout')?>">
                     <button type="submit" class="btn logout-btn" href="">Cerrar Sesión <i class="fa-solid fa-power-off"></i></button>
                  </form>
            </div>
            <div class="col-sm-12 col-md-12 col-xl-6">
               <div class="d-flex flex-column align-items-center align-items-lg-end justify-content-center h-100">
                  <div class="account-card-img"><img class="img-card" src="<?= base_url('assets/images/cards').'/'.$cliente['nivel'].'.png'?>" alt="">
                     <div class="barcode-bg"><img src="https://concierge.somosplasa.com/assets/images/barcodes/<?= $cliente['tarjeta'] ?>.png" alt="Código Tarjeta" class="barcode-img"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
<?= $this->endSection() ?>
