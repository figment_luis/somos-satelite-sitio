<?= $this->extend('layout') ?>

<!-- ========= TITLE PAGE  ========= -->
<?= $this->section('title-page') ?>Contacto<?= $this->endSection() ?>


<?= $this->section('content') ?>

<div class="container mt-5 mb-5">

    <section class='contact-section'>
        <div class="text-center">
            <h2 class='section-title text-center deco-line-center'>Contacto</h2>

            <br><br><br>
            <p><a href="mailto: plandelealtad@plazasatelite.com.mx">plandelealtad@plazasatelite.com.mx</a></p>
            <br><br><br>

            <p class="contact-title">DIRECCIÓN</p>
            <p>Cto Centro Comercial 2251, Cd. Satélite</p>
            <p>C.P. 53100 Naucalpan de Juárez, Méx.</p>

            <br><br><br>

            <p class="contact-title">CENTRO DE ATENCIÓN A CLIENTES</p>
            <a href="tel:  5559192053">55-5919-2053</a>
            <br>
            <a href="tel:  5555721400">55-5572-1400</a>
            <br><br><br>
        </div>
    </section>
    <div class="map mb-5">
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15042.919894629824!2d-99.2342176!3d19.5102499!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x95f50214869a519a!2sPlaza%20Sat%C3%A9lite!5e0!3m2!1ses-419!2smx!4v1649345524915!5m2!1ses-419!2smx" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
    </div>

</div>


<?= $this->endSection() ?>

