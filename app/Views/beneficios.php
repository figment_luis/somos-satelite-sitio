<?= $this->extend('layout') ?>

<!-- ========= TITLE PAGE  ========= -->
<?= $this->section('title-page') ?>Beneficios<?= $this->endSection() ?>


<?= $this->section('content') ?>
    <div class="bg-gray">
        <section class="container">
            <h1 class="section-title text-center deco-line-center">Beneficios</h1>
                <h2 class="text-center">Azul</h2>
                <div class="d-flex justify-content-center flex-wrap">
                    <?php if ($beneficios_azul): ?>
                        <?php foreach ($beneficios_azul as $benefit): ?>
                            <div class="card-flex mb-4">
                                <div class="card-wrap">
                                    <div class="certificate benefit">
                                        <img src="<?= 'https://concierge.somosplasa.com/' . $benefit['image'] ?>" class="img-fluid certificate__image" alt="<?= strtoupper(esc($benefit['title'])) ?>" title="<?= strtoupper(esc($benefit['title'])) ?>">
                                        <div class="certificate__info">
                                            <h5 class="certificate__title"><?= esc($benefit['title']) ?></h5>
                                            <h6 class="certificate__subtitle"><?= esc($benefit['subtitle']) ?></h6>
                                            <ul class="p-0 list-unstyled certificate__list">
                                                <!-- <li class="certificate__item"><i class="fas fa-layer-group"></i> <?php esc($benefit['nivel']) ?></li> -->
                                                <li class="certificate__item"><i class="fas fa-cubes"></i> Stock: <?= esc($benefit['unlimited_stock']) ? 'Ilimitado' : esc($benefit['current_stock']) ?></li>
                                            </ul>
                                            <p class="certificate__description"><?= esc($benefit['description']) ?></p>
                                            <?php if($benefit['rule_days'] || $benefit['rule_week'] || $benefit['rule_months'] || $benefit['rule_shop']): ?>
                                                <div class="certificate__rules">
                                                    <h6 class="certificate__rules-title" data-bs-toggle="collapse" href="<?= '#rules-' . $benefit['id'] ?>"><i class="fas fa-info-circle me-1"></i> Condiciones para entregrar este beneficio</h6>
                                                    <ul class="certificate__rules-item collapse" id="<?= 'rules-' . $benefit['id'] ?>">
                                                        <?php if ($benefit['rule_days']): ?>
                                                            <li>Limitado a <?= $benefit['rule_days'] ?> entregas por día.</li>
                                                        <?php endif; ?>
                                                        <?php if ($benefit['rule_week']): ?>
                                                            <li>Limitado a <?= $benefit['rule_week'] ?> entregas por semana.</li>
                                                        <?php endif; ?>
                                                        <?php if ($benefit['rule_months']): ?>
                                                            <li>Limitado a <?= $benefit['rule_months'] ?> entregas por mes.</li>
                                                        <?php endif; ?>
                                                        <?php if ($benefit['rule_shop']): ?>
                                                            <li>Limitado a socios que hayan acreditado un monto mínimo de $150 en compras durante los últimos 15 días.</li>
                                                        <?php endif; ?>
                                                    </ul>
                                                </div>
                                            <?php else: ?>
                                                <p class="mb-0 certificate__no-rules-title">*Sin condiciones de entrega</p>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <div class="col-12 text-center">
                            <img src="<?= base_url('assets/images/svg/404-gifts.svg')?>" alt="No hay premios registrados" class="img-fluid w-50 mb-4">
                            <h2 class="listado-premios__title-error">Upss!</h2>
                            <p class="listado-premios__message">Coméntale al cliente que todos los beneficios han volado, son muchos los recompensas que se obtienen al estar registrado en el Plan de Lealtad.
                                <br>Pero este suceso no debería preocuparle. <strong>Mañana habrá mas beneficios para entregar.</strong></p>
                        </div>
                    <?php endif; ?>
                </div>
                <hr>
                <h2 class="text-center">Verde</h2>
                <div class="d-flex justify-content-center flex-wrap">
                    <?php if ($beneficios_verde): ?>
                        <?php foreach ($beneficios_verde as $benefit): ?>
                            <div class="card-flex mb-4">
                                <div class="card-wrap">
                                    <div class="certificate benefit">
                                        <img src="<?= 'https://concierge.somosplasa.com/' . $benefit['image'] ?>" class="img-fluid certificate__image" alt="<?= strtoupper(esc($benefit['title'])) ?>" title="<?= strtoupper(esc($benefit['title'])) ?>">
                                        <div class="certificate__info">
                                            <h5 class="certificate__title"><?= esc($benefit['title']) ?></h5>
                                            <h6 class="certificate__subtitle"><?= esc($benefit['subtitle']) ?></h6>
                                            <ul class="p-0 list-unstyled certificate__list">
                                                <!-- <li class="certificate__item"><i class="fas fa-layer-group"></i> <?php esc($benefit['nivel']) ?></li> -->
                                                <li class="certificate__item"><i class="fas fa-cubes"></i> Stock: <?= esc($benefit['unlimited_stock']) ? 'Ilimitado' : esc($benefit['current_stock']) ?></li>
                                            </ul>
                                            <p class="certificate__description"><?= esc($benefit['description']) ?></p>
                                            <?php if($benefit['rule_days'] || $benefit['rule_week'] || $benefit['rule_months'] || $benefit['rule_shop']): ?>
                                                <div class="certificate__rules">
                                                    <h6 class="certificate__rules-title" data-bs-toggle="collapse" href="<?= '#rules-' . $benefit['id'] ?>"><i class="fas fa-info-circle me-1"></i> Condiciones para entregrar este beneficio</h6>
                                                    <ul class="certificate__rules-item collapse" id="<?= 'rules-' . $benefit['id'] ?>">
                                                        <?php if ($benefit['rule_days']): ?>
                                                            <li>Limitado a <?= $benefit['rule_days'] ?> entregas por día.</li>
                                                        <?php endif; ?>
                                                        <?php if ($benefit['rule_week']): ?>
                                                            <li>Limitado a <?= $benefit['rule_week'] ?> entregas por semana.</li>
                                                        <?php endif; ?>
                                                        <?php if ($benefit['rule_months']): ?>
                                                            <li>Limitado a <?= $benefit['rule_months'] ?> entregas por mes.</li>
                                                        <?php endif; ?>
                                                        <?php if ($benefit['rule_shop']): ?>
                                                            <li>Limitado a socios que hayan acreditado un monto mínimo de $150 en compras durante los últimos 15 días.</li>
                                                        <?php endif; ?>
                                                    </ul>
                                                </div>
                                            <?php else: ?>
                                                <p class="mb-0 certificate__no-rules-title">*Sin condiciones de entrega</p>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <div class="col-12 text-center">
                            <img src="<?= base_url('assets/images/svg/404-gifts.svg')?>" alt="No hay premios registrados" class="img-fluid w-50 mb-4">
                            <h2 class="listado-premios__title-error">Upss!</h2>
                            <p class="listado-premios__message">Coméntale al cliente que todos los beneficios han volado, son muchos los recompensas que se obtienen al estar registrado en el Plan de Lealtad.
                                <br>Pero este suceso no debería preocuparle. <strong>Mañana habrá mas beneficios para entregar.</strong></p>
                        </div>
                    <?php endif; ?>
                </div>
                <hr>
                <h2 class="text-center">Morado</h2>
                <div class="d-flex justify-content-center flex-wrap">
                    <?php if ($beneficios_morado): ?>
                        <?php foreach ($beneficios_morado as $benefit): ?>
                            <div class="card-flex mb-4">
                                <div class="card-wrap">
                                    <div class="certificate benefit">
                                        <img src="<?= 'https://concierge.somosplasa.com/' . $benefit['image'] ?>" class="img-fluid certificate__image" alt="<?= strtoupper(esc($benefit['title'])) ?>" title="<?= strtoupper(esc($benefit['title'])) ?>">
                                        <div class="certificate__info">
                                            <h5 class="certificate__title"><?= esc($benefit['title']) ?></h5>
                                            <h6 class="certificate__subtitle"><?= esc($benefit['subtitle']) ?></h6>
                                            <ul class="p-0 list-unstyled certificate__list">
                                                <!-- <li class="certificate__item"><i class="fas fa-layer-group"></i> <?php esc($benefit['nivel']) ?></li> -->
                                                <li class="certificate__item"><i class="fas fa-cubes"></i> Stock: <?= esc($benefit['unlimited_stock']) ? 'Ilimitado' : esc($benefit['current_stock']) ?></li>
                                            </ul>
                                            <p class="certificate__description"><?= esc($benefit['description']) ?></p>
                                            <?php if($benefit['rule_days'] || $benefit['rule_week'] || $benefit['rule_months'] || $benefit['rule_shop']): ?>
                                                <div class="certificate__rules">
                                                    <h6 class="certificate__rules-title" data-bs-toggle="collapse" href="<?= '#rules-' . $benefit['id'] ?>"><i class="fas fa-info-circle me-1"></i> Condiciones para entregrar este beneficio</h6>
                                                    <ul class="certificate__rules-item collapse" id="<?= 'rules-' . $benefit['id'] ?>">
                                                        <?php if ($benefit['rule_days']): ?>
                                                            <li>Limitado a <?= $benefit['rule_days'] ?> entregas por día.</li>
                                                        <?php endif; ?>
                                                        <?php if ($benefit['rule_week']): ?>
                                                            <li>Limitado a <?= $benefit['rule_week'] ?> entregas por semana.</li>
                                                        <?php endif; ?>
                                                        <?php if ($benefit['rule_months']): ?>
                                                            <li>Limitado a <?= $benefit['rule_months'] ?> entregas por mes.</li>
                                                        <?php endif; ?>
                                                        <?php if ($benefit['rule_shop']): ?>
                                                            <li>Limitado a socios que hayan acreditado un monto mínimo de $150 en compras durante los últimos 15 días.</li>
                                                        <?php endif; ?>
                                                    </ul>
                                                </div>
                                            <?php else: ?>
                                                <p class="mb-0 certificate__no-rules-title">*Sin condiciones de entrega</p>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <div class="col-12 text-center">
                            <img src="<?= base_url('assets/images/svg/404-gifts.svg')?>" alt="No hay premios registrados" class="img-fluid w-50 mb-4">
                            <h2 class="listado-premios__title-error">Upss!</h2>
                            <p class="listado-premios__message">Coméntale al cliente que todos los beneficios han volado, son muchos los recompensas que se obtienen al estar registrado en el Plan de Lealtad.
                                <br>Pero este suceso no debería preocuparle. <strong>Mañana habrá mas beneficios para entregar.</strong></p>
                        </div>
                    <?php endif; ?>
                </div>
                <br>

        </section>
    </div>
<?= $this->endSection() ?>


