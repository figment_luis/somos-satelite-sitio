<?= $this->extend('layout') ?>

<!-- ========= TITLE PAGE  ========= -->
<?= $this->section('title-page') ?>Descubre<?= $this->endSection() ?>


<?= $this->section('content') ?>
        <div class="seccion-descubre bg-gray">
            <section class="container">
                <h1 class="section-title text-center deco-line-center">Descubre</h1>
                <div class="row mt-5">
                    <div class="col-sm-12 col-md-6">
                        <div class="d-flex flex-column justify-content-center h-100">
                            <h3 class="subtitle">¿QUÉ ES PLA SA?</h3>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonumLorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorperLorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>
                            <br>
                            <h3 class="subtitle">¿Cómo funciona?</h3>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonumLorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorperLorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>
                        </div>
                        
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <img src="<?= base_url('assets/images/descubre-1.jpg');?>" alt="" class="img-fluid">
                    </div>
                </div>
            </section>
        </div>
        

        <section class="container steps-section">
            <h1 class="section-title text-center mb-5 deco-line-center">¿Cómo funciona?</h1>
            <div class="row mb-5">
                <div class="col-sm-12 col-md-4 d-none d-md-block">
                    <img src="<?= base_url('assets/images/descubre-2.jpg');?>" alt="" class="img-fluid">
                </div>
                <div class="col-sm-12 col-md-6 offset-md-2">
                    <div class="d-flex flex-column justify-content-end h-100">
                        <div class="d-flex align-items-center justify-content-center justify-content-md-start">
                            <span class="step-title"><i class="fa-solid fa-pen-to-square"></i></span>
                            <p class="step-description">Lorem ipsum dolor sit amet. </p>
                        </div>
                        <div class="d-flex align-items-center justify-content-center justify-content-md-start">
                            <span class="step-title"><i class="fa-solid fa-bag-shopping"></i></span>
                            <p class="step-description">Lorem ipsum dolor sit amet. </p>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="d-flex align-items-center justify-content-center justify-content-md-start">
                                <span class="step-title"><i class="fa-solid fa-receipt"></i></span>
                                <p class="step-description">Lorem ipsum dolor sit amet. </p>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="d-flex align-items-center justify-content-center justify-content-md-start">
                                <span class="step-title"><i class="fa-solid fa-gifts"></i></span>
                                <p class="step-description">Lorem ipsum dolor sit amet. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="container"><hr></div>
        

        <section class="container card-info-section">
            <h1 class="section-title text-center deco-line-center mb-5">Niveles y sus beneficios</h1>
            <div class="row">
                <?php foreach($cards as $card):?>
                    <div class="col-sm-12 col-md-4 mb-3">
                        <img src="<?= 'https://concierge.somosplasa.com/' . $card['image'] ?>" alt="" class="img-fluid mb-5">
                        <h2 class="mt-2 card-info-title"><?= $card['name']?></h2>
                        <p><?= $card['description']?></p>
                        <?php if($card['name'] == 'Blue'):?>
                            <ul>
                                <li>Invitación a eventos VIP</li>
                                <li>Invitación a ventas especiales</li>
                                <li>Descuentos y promociones especiales en nuestras marcas aliadas.</li>
                            </ul>
                        <?php elseif($card['name'] == 'Silver'):?>
                            <ul>
                                <li>Estacionamiento gratuito por 2 hrs una vez al día.*</li>
                                <li>Invitación a eventos VIP </li>
                                <li>Invitación a ventas especiales</li>
                                <li>Descuentos y promociones especiales en nuestras marcas aliadas.</li>
                            </ul>
                        <?php elseif($card['name'] == 'Gold'):?>
                            <ul>
                                <li>Servicio gratuito de Personal Shopper</li>
                                <li>Estacionamiento gratuito por 2 hrs una vez al día.*</li>
                                <li>Invitación a eventos VIP</li>
                                <li>Invitación a ventas especiales</li>
                                <li>Descuentos y promociones especiales en nuestras marcas aliadas.</li>
                            </ul>
                        <?php endif;?>
                        <br><br>
                    </div>
                <?php endforeach;?>
            </div>
        </section>
        
<?= $this->endSection() ?>
