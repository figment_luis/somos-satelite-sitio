<?= $this->extend('layout') ?>

<!-- ========= TITLE PAGE  ========= -->
<?= $this->section('title-page') ?>Premios<?= $this->endSection() ?>


<?= $this->section('content') ?>
    <div class="bg-gray py-5">
        <div class="container py-5">
            <h2 class="section-title text-center deco-line-center space-2">Premios</h2>
                <div class="row">
                <?php if ($premios): ?>
                    <?php foreach ($premios as $premio): ?>
                        <div class="col-12 col-sm-6 col-lg-4 col-xl-3 mb-4">
                            <div class="card-wrap">
                                <div class="certificate">
                                    <img src="<?= 'https://concierge.somosplasa.com/' . $premio['image'] ?>" class="img-fluid certificate__image" alt="<?= strtoupper(esc($premio['title'])) ?>" title="<?= strtoupper(esc($premio['title'])) ?>">
                                    <div class="certificate__info">
                                        <h5 class="certificate__title"><?= esc($premio['title']) ?></h5>
                                        <h6 class="certificate__subtitle"><?= esc($premio['subtitle']) ?></h6>
                                        <ul class="p-0 list-unstyled certificate__list">
                                            <li class="certificate__item"><i class="fas fa-coins"></i> Puntos: <?= number_format(esc($premio['points']),0,'.', ',') ?></li>
                                            <li class="certificate__item"><i class="fas fa-cubes"></i> Stock: <?= esc($premio['current_stock']) ?></li>
                                        </ul>
                                        <p class="certificate__description"><?= esc($premio['description']) ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php else: ?>
                    <div class="col-12 text-center">
                        <img src="<?= base_url('assets/images/svg/404-gifts.svg')?>" alt="No hay premios registrados" class="img-fluid w-50 mb-4">
                        <h2 class="listado-premios__title-error">Upss!</h2>
                        <p class="listado-premios__message">Coméntale al cliente que todos los certificados han volado, son muchas las recompensas que se obtienen al estar registrado en el Plan de Lealtad.
                            <br>Pero este suceso no debe preocuparle. <strong>Mañana habrá mas certificados para canjerar.</strong></p>
                    </div>
                <?php endif; ?>
                </div>
        </div>
    </div>
    
<?= $this->endSection() ?>


