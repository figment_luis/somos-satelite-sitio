<?= $this->extend('layout') ?>

<!-- ========= TITLE PAGE  ========= -->
<?= $this->section('title-page') ?>FAQs<?= $this->endSection() ?>


<?= $this->section('content') ?>
    <div class="container pt-5">
        <h1 class="text-center">FAQS</h1>
        <hr>

        <h3 class="mb-5 mt-5">¿QUÉ ES SOMOS PLA·SA?</h3>
        <p class="mb-5">SOMOS PLA·SA es un programa de fidelidad para los clientes de Plaza Satélite que te da ventajas y privilegios únicos para ti.</p>

        <h3 class="mb-5">¿Cómo obtengo una tarjeta?</h3>
        <p class="mb-5">Para obtener una tarjeta SOMOS PLA·SA, usted sólo necesita acudir al módulo de Concierge con tickets de compras correspondientes al mismo día de su visita por un monto mínimo total de $500.00 pesos.</p>

        <h3 class="mb-5">¿Tiene algún costo la tarjeta SOMOS PLA·SA?</h3>
        <p class="mb-5">No, la inscripción al programa de recompensas SOMOS PLA·SA es totalmente gratuita.</p>

        <h3 class="mb-5">¿Qué niveles tiene SOMOS PLA·SA?</h3>
        <p class="mb-5">SOMOS PLA·SA tiene tres niveles de membresía: Azul, Verde y Morado.</p>

        <h3 class="mb-5">¿Cómo me asignan mi nivel en mi primer compra?</h3>
        <p class="mb-5">Con sus tickets de compra del mismo día, de acuerdo a los siguientes montos se le asigna su nivel:</p>
        <ul class="mb-5">
            <li>Azul = $500 - $4,999</li>
            <li>Verde = $5000 - $19,999</li>
            <li>Morado = Compras mayores a $20,000 </li>
        </ul>

        <h3 class="mb-5">¿Cómo subo de nivel SOMOS PLA·SA?</h3>
        <p class="mb-5">Si usted acumula más puntos de los indicados en un nivel SOMOS PLA·SA y en el periodo de compra especificados, automáticamente obtendrá un upgrade al siguiente nivel SOMOS PLA·SA.</p>
        
        <p class="mb-5">Cuando logre un upgrade, deberá de cumplir el monto de compra del nivel en el periodo asignado para que se respete al siguiente periodo, si no es así se bajará al nivel que correspondan sus compras. El nivel base siempre será Básico</p>

        <h3 class="mb-5">¿Qué boutiques y tiendas participan en SOMOS PLA·SA?</h3>
        <p class="mb-5">Participan todas las tiendas, boutiques, restaurantes y entretenimiento que están físicamente dentro de Plaza Satélite, los espacios que no participan en el programa son las tiendas temporales dentro de Plaza Satélite.</p>

        <h2 class="mb-5">Puntos SOMOS PLA·SA</h2>

        <h3 class="mb-5">¿Cómo acumulo puntos?</h3>
        <p class="mb-5">Por cada $50 pesos de compra en Plaza Satélite se acreditarán puntos SOMOS PLA·SA según la siguiente tabla de equivalencia:</p>
        <br>
        <p class="mb-3">Tarjeta Azul 1 punto por cada $20 pesos de compra en Plaza Satélite.</p>
        <p class="mb-3">Tarjeta Verde 2 puntos por cada $20 pesos de compra en Plaza Satélite.</p>
        <p class="mb-3">Tarjeta Morada 3 puntos por cada $20 pesos de compra en Plaza Satélite.</p>

        <h3 class="mb-5">¿A partir de qué fecha puedo comenzar a acumular puntos?</h3>
        <p class="mb-5">A partir del momento en que se le entrega la tarjeta, las compras anteriores a la entrega de la tarjeta no podrán ser registradas.</p>

        <h3 class="mb-5">¿Por cuánto tiempo son válidos mis puntos?</h3>
        <p class="mb-5">Los puntos tienen una vigencia de año calendario. Todos los puntos vencerán el último día de febrero del año siguiente al de registro. Aplica a todos los usuarios, incluidos nuevos o recientes.</p>

        <h3 class="mb-5">¿Dónde registro mis compras para acumular puntos?</h3>
        <p class="mb-5">Visitando el módulo de Concierge con sus tickets de compra y tarjeta SOMOS PLA·SA, el Concierge acreditará los puntos y sellará los tickets de compra.</p>

        <h3 class="mb-5">¿Qué tiendas pueden acreditarme mis puntos?</h3>
        <p class="mb-5">Ninguna. Todas las compras y consumos realizados en tiendas, boutiques, restaurantes y locales deben ser acreditadas en el módulo de Concierge.</p>

        <h3 class="mb-5">¿Qué hago si olvidé acreditar mis puntos el día que hice mis compras?</h3>
        <p class="mb-5">Usted tiene 15 días naturales posteriores a su compra para pasar al módulo de Concierge con su ticket de compra donde con gusto le acreditarán sus puntos, una vez transcurrido ese tiempo ya no será posible realizar la acreditación.</p>

        <h3 class="mb-5">¿Puedo transferir mis puntos?</h3>
        <p class="mb-5">No, los puntos SOMOS PLA·SA no son transferibles entre miembros SOMOS PLA·SA.</p>

        <h3 class="mb-5">¿Puedo acreditar compras que no estén a mi nombre o que yo no haya realizado personalmente?</h3>
        <p class="mb-5">Sí, pero una vez registrada la compra en SOMOS PLA·SA, dicha compra no podrá ser registrada para ningún otro socio.</p>

        <h3 class="mb-5">¿Qué puedo hacer con mis puntos SOMOS PLA·SA?</h3>
        <p class="mb-5">Por favor, pase a Concierge de Plaza Satélite o puede visitar la pagina https://somosplasa.com/premios para consultar el catálogo de redención vigente y/o redimir sus puntos.</p>

        <h2 class="mb-5">Estado de cuenta SOMOS PLA·SA</h2>
        
        <h3 class="mb-5">¿Qué es una transacción?</h3>
        <p class="mb-5">Son las distintas compras que usted ha realizado y que ya generan Puntos Firmes o en Transición.</p>

        <h3 class="mb-5">¿En cuánto tiempo veré las transacciones SOMOS PLA·SA en mi estado de cuenta?</h3>
        <p class="mb-5">De 1 a 2 días después de realizada su compra.</p>
        
        <h3 class="mb-5">¿Cómo está formado mi estado de cuenta?</h3>
        <p class="mb-5">El estado de cuenta está formado por:</p>
        <br>
        <p class="mb-5">Nombre: Nombre registrado en el plan de recompensas</p>
        <p class="mb-5">Tarjeta: Numero de tarjeta utilizado habilitada en el plan de recompensas.</p>
        <p class="mb-5">Tipo de Cliente: Nivel que se tiene de acuerdo al historial de compras.</p>
        <p class="mb-5">Puntos totales: Suma de los puntos firmes y en transición.</p>

        
        <h3 class="mb-5">¿Qué son las redenciones SOMOS PLA·SA?</h3>
        <p class="mb-5">Son aquellos canjes realizados con sus puntos por experiencias o premios.</p>

        <h2 class="mb-5">Beneficios y Experiencias</h2>
        
        <h3 class="mb-5">¿Qué son las experiencias?</h3>
        <p class="mb-5">Por favor, pase a el módulo de Concierge o puede visitar la pagina https://somosplasa.com/premios para consultar el catálogo de redención vigente y/o redimir sus puntos.</p>
        
        <h3 class="mb-5">¿Dónde colecto mis premios y certificados para experiencias?</h3>
        <p class="mb-5">Los premios y experiencias se entregan sólo al titular de la membresía presentando el comprobante electrónico e identificación oficial vigente en el módulo de Concierge.</p>
        
        <h3 class="mb-5">¿Cuánto tiempo tengo para reclamar mis experiencias o premios?</h3>
        <p class="mb-5">La vigencia varía en cada una de las experiencias y premios, esta se especifica al momento de la entrega en el certificado otorgado.</p>
        
        <h3 class="mb-5">¿Cómo funcionan los beneficios?</h3>
        <p class="mb-5">Por favor, pase a el módulo de Concierge  o puede visitar la pagina https://somosplasa.com/beneficios para que le sean proporcionados los beneficios vigentes</p>
        
        <h3 class="mb-5">¿Cómo hago válidos los beneficios?</h3>
        <p class="mb-5">Para hacer válidos los beneficios SOMOS PLA·SA, sólo presente en la tienda su tarjeta SOMOS PLA·SA y solicite el beneficio, algunos beneficios requieren de una acreditación de puntos menor a 15 días.</p>
        
        <h3 class="mb-5">¿Los beneficios son transferibles?</h3>
        <p class="mb-5">No, éstos son únicamente para el titular de la tarjeta, la cual debe ser firmada y acompaña con la muestra de una identificación oficial para recibir el beneficio.</p>
        
        <h3 class="mb-5">¿Dónde puedo reportar beneficios no respetados o deficiencias del programa?</h3>
        <p class="mb-5">Usted puede reportar cualquier beneficio no respetado o malfuncionamiento del programa en el Módulo de Concierge o al 55-5919-2053.</p>

        <h2 class="mb-5">Métodos de pago</h2>
        
        <h3 class="mb-5">¿Qué métodos de pago son aceptados para generar puntos SOMOS PLA·SA?</h3>
        <p class="mb-5">Son métodos válidos de pago para acumular puntos SOMOS PLA·SA, aquellas compras realizadas, en efectivo, tarjetas de crédito y débito</p>
        
        <h3 class="mb-5">¿Cómo funcionan las tarjetas de regalo con SOMOS PLA·SA?</h3>
        <p class="mb-5">Las tarjetas de regalo no son un medio válido de pago para acumular puntos SOMOS PLA·SA.</p>

        <h2 class="mb-5">CONCIERGE SOMOS PLA·SA</h2>
        
        <h3 class="mb-5">¿Qué servicios proporciona el Concierge?</h3>
        <p class="mb-5">El  Concierge le puede proporcionar información y servicios de:</p>
        <ul class="mb-5">
            <li>Tiendas y boutiques ubicadas en el Centro Comercial.</li>
            <li>Promociones SOMOS PLA·SA y recomendaciones SOMOS PLA·SA.</li>
            <li>Información en general del programa de recompensas SOMOS PLA·SA.</li>
            <li>Acreditación de puntos SOMOS PLA·SA.</li>
            <li>Acreditación de promociones SOMOS PLA·SA.</li>
            <li>Entrega y registro de membresía SOMOS PLA·SA.</li>
        </ul>

        <h3 class="mb-5">¿En qué horarios puedo acreditar mis puntos en  Concierge?</h3>
        <p class="mb-5">El horario de Concierge es lunes a viernes de 10:00 de la mañana a 7:00 de la noche, Sábados y Domingos de 11:00 de la mañana a 8:00 de la noche, horario en el que usted podrá acreditar sus puntos.</p>

        <h2 class="mb-5">Ayuda</h2>
        
        <h3 class="mb-5">¿A quién contacto en caso de tener dudas o requerir ayuda?</h3>
        <p class="mb-5">Usted puede aclarar sus dudas en cualquier momento en el módulo de Concierge o en el teléfono del Centro de Atención a Clientes 55-5919-2053.</p>

        <h3 class="mb-5">¿Qué hago en caso de extraviar mi tarjeta SOMOS PLA·SA?</h3>
        <p class="mb-5">Reporte su tarjeta SOMOS PLA·SA de inmediato a Concierge , donde ésta será cancelada y le será sustituida por una nueva.</p>

        <h3 class="mb-5">¿SOMOS PLA·SA se hace responsable de mis puntos si extravío mi tarjeta y estos son usados?</h3>
        <p class="mb-5">Si su tarjeta ha sido robada o extraviada es obligación del socio notificarlo oportunamente ya que SOMOS PLA·SA no se hace responsable por el mal uso de sus puntos.</p>

        <h3 class="mb-5">¿Sucede algo con mi membresía si no la uso?</h3>
        <p class="mb-5">Si el socio no genera ningún movimiento durante 24 meses, su participación se dará por cancelada con previo aviso.</p>

        <h3 class="mb-5">¿Pueden ser suspendidos los beneficios de mi tarjeta?</h3>
        <p class="mb-5">Sí, el programa SOMOS PLA·SA, podrá suspender en cualquier momento los beneficios de una tarjeta por así convenir a sus intereses o por mal uso de la misma, en virtud que los beneficios son intransferibles.</p>

        <h3 class="mb-5">¿Puede por algún motivo ser cancelada mi membresía SOMOS PLA·SA?</h3>
        <p class="mb-5">Sí, el programa SOMOS PLA·SA se reserva el derecho de cancelar la membresía a cualquier socio que a juicio del programa SOMOS PLA·SA haya realizado o sea sospechoso (a entero criterio del programa SOMOS PLA·SA) de fraude, abuso o violación a alguno de los créditos de puntos, beneficios, uso de recompensas o cualquier otra reglamentación del programa SOMOS PLA·SA.</p>

        <h3 class="mb-5">¿Puedo ser sujeto a auditoría para asegurar el buen uso de la tarjeta?</h3>
        <p class="mb-5">Sí, la administración de LIMITLESS Plaza Satélite realizará auditorías constantes para asegurar el buen uso de las tarjetas, puntos y redención de premios o experiencias.</p>

        <h3 class="mb-5">¿SOMOS PLA·SA compartirá o venderá mis datos de contacto?</h3>
        <p class="mb-5">Los datos proporcionados por los socios durante la inscripción son resguardados por el programa de recompensas SOMOS PLA·SA, pudiendo ser utilizados datos estadísticos para la mejora del programa y para comunicación de SOMOS PLA·SA y Plaza Satélite. Aviso de Privacidad.</p>

        <h3 class="mb-5">Trabajo en una boutique o tienda de Plaza Satélite, ¿puedo participar en SOMOS PLA·SA?</h3>
        <p class="mb-5">Sí puedes ser parte de SOMOS PLA·SA, siempre y cuando tus tickets de compra no sean de la boutique o tienda donde se está laborando actualmente. El Programa SOMOS PLA·SA se reserva el derecho de cancelar la membresía a cualquier socio que a juicio del programa SOMOS PLA·SA haya realizado o sea sospechoso (a entero criterio del Programa SOMOS PLA·SA) de fraude, abuso o violación a alguno de los créditos de puntos, beneficios, uso de recompensas o cualquier otra reglamentación del programa SOMOS PLA·SA.</p>
    </div>

<?= $this->endSection() ?>


