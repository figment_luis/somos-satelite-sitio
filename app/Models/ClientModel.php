<?php

namespace App\Models;

use CodeIgniter\Model;

class ClientModel extends Model
{
    protected $DBGroup = 'default';
    protected $table      = 'clients';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'password'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;


    public function getClientInfo($id){
        $query= $this->query("SELECT CONCAT_WS(' ', c.first_name, c.last_name) nombre, c.email, cards.number tarjeta, cl.name nivel, cl.image imagen FROM clients c LEFT JOIN cards ON cards.client_id = c.id LEFT JOIN card_levels cl ON cl.id=cards.card_level_id WHERE c.id = $id");
        return $query->getResultArray();
    }

    public function getPoints($client_id){
        $query= $this->query("SELECT SUM(points_available) as points_available, SUM(points_redeemed) as points_redeemed, SUM(points_debt) as points_debt, id as client_id FROM transactions WHERE client_id = $client_id AND enabled = 1 AND is_canceled = 0");
        return $query->getResultArray();
    }

    public function getClientByEmail($email){
        $query= $this->query("SELECT * FROM clients WHERE email = '$email'");
        return $query->getResultArray();
    }
}
