<?php

namespace App\Models;

use CodeIgniter\Model;

class CertificatesModel extends Model
{
    protected $DBGroup = 'default';
    protected $table      = 'certificates';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    public function getAllCertificates(){
        $query= $this->query("SELECT * FROM certificates WHERE deleted_at IS NULL AND current_stock >= 1 AND enabled = 1 ORDER BY id DESC");
        return $query->getResultArray();
    }

    public function getlastCertificates(){
        $query= $this->query("SELECT * FROM certificates WHERE deleted_at IS NULL AND current_stock >= 1 AND enabled = 1 ORDER BY id DESC LIMIT 8");
        return $query->getResultArray();
    }
}
