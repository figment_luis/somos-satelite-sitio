<?php

namespace App\Models;

use CodeIgniter\Model;

class CardsModel extends Model
{
    protected $DBGroup = 'default';
    protected $table      = 'cards';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    public function getLevelCards(){
        $query= $this->query("SELECT * FROM card_levels");
        return $query->getResultArray();
    }
}
