<?php

namespace App\Models;

use CodeIgniter\Model;

class SlidesModel extends Model
{
    protected $DBGroup = 'dbweb';
    protected $table      = 'slides';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
    ];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    public function getSlides(){
        $query= $this->query("SELECT * FROM slides where enabled = 1 order by position");
        return $query->getResultArray();
    }
}
