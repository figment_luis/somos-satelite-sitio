<?php

namespace App\Models;

use CodeIgniter\Model;

class BenefitsModel extends Model
{
    protected $DBGroup = 'default';
    protected $table      = 'benefits';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    public function getAllBenefits(){
        $query= $this->query("SELECT b.*, MIN(l.level_id) as 'id_nivel', cl.name as nivel FROM benefits b LEFT JOIN benefit_levels l on b.id = l.benefit_id LEFT JOIN card_levels cl on cl.id = l.level_id  WHERE b.deleted_at IS NULL AND (b.unlimited_stock = 1 OR b.current_stock >= 1) AND b.enabled = 1 GROUP BY b.title");
        return $query->getResultArray();
    }

    public function getLevels(){
        $query= $this->query("SELECT * FROM benefit_levels");
        return $query->getResultArray();
    }

    public function getlastBenefits(){
        $query= $this->query("SELECT * FROM benefits WHERE deleted_at IS NULL AND (unlimited_stock = 1 OR current_stock >= 1) AND enabled = 1 ORDER BY id DESC LIMIT 4");
        return $query->getResultArray();
    }

}
