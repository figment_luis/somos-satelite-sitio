<?php

namespace App\Models;

use CodeIgniter\Model;

class PopupModel extends Model
{
    protected $DBGroup = 'dbweb';
    protected $table      = 'popups';
    protected $primaryKey = 'id';

    protected $returnType     = 'object';
    protected $useSoftDeletes = false;

    protected $allowedFields = [];

    public function getPopups()
    {
        return $this->query("SELECT * FROM popups where enabled = 1")->getRow();
    }

}