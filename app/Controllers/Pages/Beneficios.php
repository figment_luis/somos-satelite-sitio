<?php

namespace App\Controllers\PAges;
use App\Controllers\BaseController;

class Beneficios extends BaseController{
    
    public $session = null;

	public function __construct(){
		$this->session = \Config\Services::session();
	}

    public function index()
    {

        $benefitsModel = model('App\Models\Services\BenefitsModel');

        $benefits = $benefitsModel->getAllBenefits();

        $beneficios_azul = [];
        $beneficios_verde = [];
        $beneficios_morado = [];

        foreach ($benefits as $benefit) {
            if ($benefit['id_nivel'] == 1) array_push($beneficios_azul, $benefit);
            if ($benefit['id_nivel'] == 2) array_push($beneficios_verde, $benefit);
            if ($benefit['id_nivel'] == 3) array_push($beneficios_morado, $benefit);
        }


        $uri = service('uri');

		$data['uri']=$uri;
        $data['beneficios_azul'] = $beneficios_azul;
        $data['beneficios_verde'] = $beneficios_verde;
        $data['beneficios_morado'] = $beneficios_morado;
        $data['session'] = $this->session->get('login');
        $data['name'] = $this->session->get('first_name');

        return view('beneficios', $data);
    }

}