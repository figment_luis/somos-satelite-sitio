<?php

namespace App\Controllers\PAges;
use App\Controllers\BaseController;

class Conocenos extends BaseController{
    
    public $session = null;

	public function __construct(){
		$this->session = \Config\Services::session();
	}

    public function index()
    {

        $cardsModel = model('App\Models\Services\CardsModel');
        

        
        $uri = service('uri');

		$data['uri']=$uri;
        $data['cards'] = $cardsModel->getLevelCards();
        $data['session'] = $this->session->get('login');
        $data['name'] = $this->session->get('first_name');

        return view('conocenos', $data);
    }

}