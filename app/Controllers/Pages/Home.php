<?php

namespace App\Controllers\Pages;
use App\Controllers\BaseController;

class Home extends BaseController{
    
    public $session = null;

	public function __construct(){
		$this->session = \Config\Services::session();
	}

    public function index()
    {

        
        $agent = $this->request->getUserAgent();

        if ($agent->isMobile()) {
            $data['mobile']=true;
        } else {
            $data['mobile']=false;
        }


        $certificatesModel = model('App\Models\Services\CertificatesModel');
        $slidesModel = model('App\Models\Services\SlidesModel');
        $popupModel = model('App\Models\PopupModel');


        $uri = service('uri');

		$data['uri']=$uri;
        $data['premios'] = $certificatesModel->getlastCertificates();
        $data['slides'] = $slidesModel->getSlides();
        $data['session'] = $this->session->get('login');
        $data['name'] = $this->session->get('first_name');
        $data['popup'] = $popupModel->getPopups();
        
        return view('home', $data);
    }

}