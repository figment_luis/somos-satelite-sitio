<?php

namespace App\Controllers\Pages;
use App\Controllers\BaseController;

class EstadoDeCuenta extends BaseController{

    public $session = null;

	public function __construct(){
		$this->session = \Config\Services::session();
	}
    
    public function index()
    {
        $data['session'] = $this->session->get('login');
        $data['name'] = $this->session->get('first_name');

        if($this->session->get('login')){

            $clientModel = model('App\Models\Services\ClientModel');


            $uri = service('uri');

		    $data['uri']=$uri;
            $data['cliente'] = $clientModel->getClientInfo($this->session->get('id'))[0];
            $data['puntos'] = $clientModel->getPoints($this->session->get('id'))[0];

            return view('estado-de-cuenta',$data);
        } else {
            return redirect()->to('home');
        }
        
    }
}