<?php

namespace App\Controllers\PAges;
use App\Controllers\BaseController;

class Faqs extends BaseController{
    
    public $session = null;

	public function __construct(){
		$this->session = \Config\Services::session();
	}

    public function index()
    {

        $uri = service('uri');

		$data['uri']=$uri;
        $data['session'] = $this->session->get('login');
        $data['name'] = $this->session->get('first_name');

        return view('faqs', $data);
    }

}