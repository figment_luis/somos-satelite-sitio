<?php

namespace App\Controllers\Services;
use App\Controllers\BaseController;

class PasswordRecovery extends BaseController{
    private $session = null;

    public function __construct()
    {
        $this->session = \Config\Services::session();
    }


	public function sendRecoveryEmail(){
        $clientModel = model('App\Models\ClientModel');
		$recoveryModel = model('App\Models\RecoveryModel');


        try{
            $client_email = filter_var( $this->request->getPost('client_email'), FILTER_SANITIZE_EMAIL );

            $client = $clientModel->getClientByEmail($client_email);

            if ($client){
                //Set data for recovery password Table
                $newCode = generate_string();
                $recoveryTableData = [
                    'client_id' => $client[0]['id'], 
                    'hash' => $newCode
                ];

                $recoveryModel->insert($recoveryTableData);
                $dataCode['hash'] = $newCode;

                //Email Setup
                $email = \Config\Services::email();
                $email->setFrom('no-reply@kinetiq.com.mx', 'Loyalty 2.0');
                $email->setTo($client_email);
                $email->setSubject('Loyalty 2.0 - Recuperación de contraseña');
                $message = view('Templates/passwordRecoveryTemplate', $dataCode);
                $email->setMessage( $message );

                if ($email->send()){
                    $this->response->setStatusCode(200);
                    $data = [
                        'status' => true,
                        'message' => 'Pronto recibirá por correo eletrónico las instrucciones para actualizar su contraseña'
                    ];
                    return $this->response->setJSON($data);
                }else {
                    $this->response->setStatusCode(500);
                    $data = [
                        'status' => false,
                        'message' => 'Ha ocurrido une error inesperado, favor de contactar con el adminsitrador del sistema'
                    ];
                    return $this->response->setJSON($data);
                }
                die(json_encode($response));
            } else {
                //No se encontro el email ingresado
                $this->response->setStatusCode(404);
                $data = [
                    'status' => false,
                    'message' => 'El correo ingresado no se encuentra dado de alta en el sistema'
                ];
                return $this->response->setJSON($data);
            }
        } catch (\Exception $e){

            $this->response->setStatusCode(500);
            $data = [
                'status' => false,
                'message' => $e->getMessage(),
            ];
            return $this->response->setJSON($data);
        }
    }


    public function updatePassword($code = NULL){

		$recoveryModel = model('App\Models\RecoveryModel');

        $uri = service('uri');

		$data['uri']=$uri;
        $data['session'] = $this->session->get('login');
        $data['name'] = $this->session->get('first_name');
        
		$tabledata = $recoveryModel->where('hash', $code)->find();

        $create_date = date_create($tabledata[0]['created_at']);
        $current_date = date_create();
        $days_diff = $create_date->diff($current_date);



	
        //Si el token esta deshabilitado o si el token tiene mas de 3 dias
		if($tabledata[0]['enabled'] == 0 OR $days_diff->days > 3){
			$data['client_id'] = false;
		} else {
			$data['client_id'] = $tabledata[0]['client_id'];
		}

		$updateCode = [
			'enabled' => 0
		];

		$recoveryModel->update($tabledata[0]['id'], $updateCode);

        return view('password-recovery', $data);
	}

    public function setNewPassword(){

		$clientModel = model('App\Models\clientModel');

		$client_password = filter_var( $this->request->getPost('client_password'), FILTER_SANITIZE_STRING );
        $client_password_confirm = filter_var( $this->request->getPost('client_password_confirm'), FILTER_SANITIZE_STRING );

        try{
            if($client_password != $client_password_confirm){
                $this->response->setStatusCode(200);
                $data = [
                    'status' => false,
                    'message' => 'Las contraseñas ingresadas deben ser iguales'
                ];
                return $this->response->setJSON($data);
            }
    
            $client_id = filter_var( $this->request->getPost('client_id'), FILTER_SANITIZE_NUMBER_INT );
    
            //Set data for new password in user Table
            $options = array(
                'cost' => 12
            );
            $password_hashed = password_hash($client_password, PASSWORD_BCRYPT, $options);
            

            $db = \Config\Database::connect();
            $builder = $db->table('clients');

            $builder->set('password', $password_hashed);
            $builder->where('id', $client_id);
            $builder->update();

    
            $this->response->setStatusCode(200);

            $data = [
                'status' => true,
                'message' => 'Ha actualizado su contraseña exitosamente'
            ];

            return $this->response->setJSON($data);

            
        } catch (\Exception $e){

            $this->response->setStatusCode(503);
            $data = [
                'status' => false,
                'message' => $e->getMessage(),
            ];
            return $this->response->setJSON($data);
        }

       
	}
}