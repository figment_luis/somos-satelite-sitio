<?php

namespace App\Controllers\Services;
use App\Controllers\BaseController;

class Login extends BaseController
{
    private $session = null;

    public function __construct()
    {
        $this->session = \Config\Services::session();
    }


	public function loginValidation()
	{
            //Array errors
            $data['errors'] = [];
            
            //Load Models
            $loginModel = model('App\Models\Services\LoginModel'); 

            //Extract Form Data
            $email = filter_var($this->request->getPost('email'), FILTER_SANITIZE_EMAIL);
            $password = filter_var($this->request->getPost('password'), FILTER_SANITIZE_STRING);

            if(!$email){ $data['errors']['email'] = 'El email debe ser obligatorio'; }

            if(!$password){ $data['errors']['password'] = 'La contraseña es obligatoria'; }

            if($data['errors']){

                $data['message'] = 'Error de validación de datos';
                $data['status'] = false;
                $data['code'] = 406;

            } else {    //If input has no errors

                try {

                    //Get the Login info
                    $loginInfo =  $loginModel->getLoginInfo($email);

                    if($loginInfo[0] AND password_verify(sha1($password), $loginInfo[0]['password'])){
                        
                        //Prepare all data session
                        $dataSesion=[
                            'id' => $loginInfo[0]['id'],
                            'first_name' => $loginInfo[0]['first_name'],
                            'last_name' => $loginInfo[0]['last_name'],
                            'email' => $loginInfo[0]['email'],
                            'login' => true
                        ];

                        //Set the data session
                        $this->session->set($dataSesion);

                        //Set the response
                        $data['status'] = true;
                        $data['code'] = 200;
                    }
                    else {
                        $data['status'] = $loginInfo[0]['password'];
                        $data['code'] = 400;
                        $data['message'] = 'Credenciales de accesos incorrectas';
                    }

                } catch (\Exception $e){
                    $data['status'] = false;
                    $data['message'] = $e->getMessage();
                    $data['code'] = 500;
                }
            }
        header("Content-type: application/json; charset=utf-8");
		die(json_encode($data, JSON_UNESCAPED_UNICODE));
	}

    public function logout()
    {
        $session = session();
        $session->destroy();
        return redirect('home');
    }
}