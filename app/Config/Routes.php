<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.

//Fontend Routes
$routes->get('/', 'Pages\Home::index');
$routes->get('/home', 'Pages\Home::index');
$routes->get('/estado-de-cuenta', 'Pages\EstadoDeCuenta::index');
$routes->get('/beneficios', 'Pages\Beneficios::index');
$routes->get('/premios', 'Pages\Premios::index');
$routes->get('/conocenos', 'Pages\Conocenos::index');
$routes->get('/contacto', 'Pages\Contacto::index');
$routes->get('/privacidad', 'Pages\Privacidad::index');
$routes->get('/faqs', 'Pages\Faqs::index');

//$routes->get('/email', 'Pages\Privacidad::email');

//Backend Routes

$routes->post('/loginValidation', 'Services\Login::loginValidation');
$routes->get('/logout', 'Services\Login::logout');
$routes->post('/api/v1/recovery-password', 'Services\PasswordRecovery::sendRecoveryEmail');
$routes->get('/update-password/(:segment)', 'Services\PasswordRecovery::updatePassword/$1');
$routes->post('/setNewPassword', 'Services\PasswordRecovery::setNewPassword');



/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
