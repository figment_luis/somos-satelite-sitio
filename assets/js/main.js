document.addEventListener("DOMContentLoaded", function() {


    let script_tag = document.getElementById('main-js');
    let url = script_tag.getAttribute('url');
    
    //Mobile Slides fix height
    
    let vh = Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0);
    let wrapper = document.getElementById("home-wrapper")
    if(wrapper){
      document.getElementById("home-wrapper").style.height= `${vh-70}px`;
    }

    if(this.location.hash){
      let recoveryModal = document.getElementById('link-modal-password');
      recoveryModal.click();
    }

    

    let homeSwiper = new Swiper('.home-swiper', {
        // Optional parameters
        loop: true,
        centeredSlides: true,
        autoplay: {
          delay: 3500,
          disableOnInteraction: false,
        },
        pagination: {
          el: ".swiper-pagination",
          clickable: true
        }
    });

    let newsSwiper = new Swiper('.news-swiper', {
      loopFillGroupWithBlank: true,
      breakpoints: {
        320: {
          slidesPerView: 1,
          slidesPerGroup: 1,
          spaceBetween: 30,
        },
        720: {
          slidesPerView: 2,
          slidesPerGroup: 2,
          spaceBetween: 30
        },
        968: {
          slidesPerView: 3,
          slidesPerGroup: 3,
          spaceBetween: 30
        },
        1400: {
          slidesPerView: 4,
          slidesPerGroup: 4,
          spaceBetween: 30
        }
      },
      navigation: {
          nextEl: ".swiper-button-next",
          prevEl: ".swiper-button-prev",
      },

    });
    

    document.querySelector("#loginForm").addEventListener("submit", function(e){
      e.preventDefault()
      e.stopPropagation()

      let myForm = document.getElementById('loginForm');
      let formData = new FormData(myForm);

      console.log(formData);

      fetch(url+'/loginValidation',{
        method: 'post',
        body: formData,
      })
      .then(response => response.json())
      .then(data => {
        if(data.code == 200){
          window.location.href = url+'/estado-de-cuenta';
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Error de acceso',
            text: 'Por favor verifique su correo y su contraseña',
          })
        }
      });
    });

    document.querySelector("#passwordRecoveryForm").addEventListener("submit", function(e){
      e.preventDefault()
      e.stopPropagation()

      let myForm = document.getElementById('passwordRecoveryForm');
      let formData = new FormData(myForm);

      fetch(url+'/api/v1/recovery-password',{
        method: 'post',
        body: formData,
      })
      .then(response => response.json())
      .then(data => {
        if(data.status == true){
          Swal.fire({
            icon: 'success',
            title: data.message,
            showConfirmButton: false
          });
         
        } else {
          Swal.fire({
            icon: 'success',
            title: 'Correo enviado',
            text: 'Gracias, se ha enviado el correo de recuperación.',
          })
        }
      });
    });

    let newPAssword = document.querySelector("#set-new-password")
    if(newPAssword){
      document.querySelector("#set-new-password").addEventListener("submit", function(e){
        e.preventDefault()
        e.stopPropagation()
    
        let myForm = document.getElementById('set-new-password');
        let formData = new FormData(myForm);
    
        console.log(formData);
    
        fetch(url+'/setNewPassword',{
          method: 'post',
          body: formData,
        })
        .then(response => response.json())
        .then(data => {
          if(data.status){
            Swal.fire({
              icon: 'success',
              title: 'Ha actualizado su contraseña exitosamente',
              showConfirmButton: false
            });
            setTimeout(() => {
              window.location.href = url;
            }, 2000);
          } else {
            Swal.fire({
              icon: 'error',
              title: 'Error',
              text: data.message,
            })
          }
        });
      });
    }
    

    let scrollDown = document.getElementById("scroll-down")
    if(scrollDown)
    {
      scrollDown.addEventListener("click", function(e){
        let height = window.innerHeight;

        window.scrollTo({
         top: height -85,
         behavior: "smooth"
        });
      });
    }

    let scrollUp = document.getElementById("scroll-up")
    if(scrollUp)
    {
      scrollUp.addEventListener("click", function(e){
        window.scrollTo({
         top: 0,
         behavior: "smooth"
        });
      });
    }

    window.addEventListener('scroll', function(e) {
      let last_known_scroll_position = window.scrollY;
      let height = window.innerHeight;
      if(last_known_scroll_position > height - 88){
        document.getElementById("scroll-up").classList.add('active');
          
      } else {
        document.getElementById("scroll-up").classList.remove('active');
      }
    });


    let navMenu = document.getElementById("navbar-menu")
    if(navMenu)
    {
      navMenu.addEventListener("click", function(e){
        let menu = document.getElementById("navbar-menu");
        if(menu.classList.contains('active')){
          document.getElementById("navbar-menu").classList.remove('active');
          document.getElementById("menu-content").classList.remove('active');
          document.getElementById("main-navbar").classList.remove('hide');
          document.getElementById("main-navbar").classList.add('shadow');
          document.getElementById("navbar-logo").style.filter="none";
          document.getElementById("navbar-session-btn").classList.add('d-md-block');
        } else {
          document.getElementById("navbar-menu").classList.add('active');
          document.getElementById("menu-content").classList.add('active');
          document.getElementById("main-navbar").classList.add('hide');
          document.getElementById("main-navbar").classList.remove('shadow');
          document.getElementById("navbar-logo").style.filter="brightness(0) invert(1)";
          document.getElementById("navbar-session-btn").classList.remove('d-md-block');
        }
      });
    }
    
});